#include "script_component.hpp"

class CfgPatches
{
	class ADDON
	{
		addonRootClass = QUOTE(MAIN_ADDON);

		name = QUOTE(COMPONENT_NAME);

		// Used for forcing load order
		requiredAddons[] = {QUOTE(MAIN_ADDON)};
	};
};

class CfgHintCategories
{
	class TCP_FM_TCP
	{
		displayName = "$STR_TCP_Language_FM_TCP_displayName";
		logicalOrder = 45;
	};
};

class CfgHints
{
	class TCP_FM_TCP2
	{
		displayName = "$STR_TCP_Language_FM_TCP_displayName";
		logicalOrder = 1;
		category = "TCP_FM_TCP";

		class TCP_FM_about
		{
			displayName = "$STR_TCP_Language_FM_about_displayName";
			description = "$STR_TCP_Language_FM_about_description";
			tip = "";
			arguments[]={};
			image = QPATHTOEF(data,Logos\arma3_tcp_icon_hint_ca.paa);
			logicalOrder = 1;
		};
	};
	
	class TCP_FM_weapons
	{
		displayName = "$STR_TCP_Language_FM_weapons_displayName";
		logicalOrder = 5;
		category = "TCP_FM_TCP";
	};

	class TCP_FM_vehicles
	{
		displayName = "$STR_TCP_Language_FM_vehicles_displayName";
		logicalOrder = 10;
		category = "TCP_FM_TCP";
	};
};